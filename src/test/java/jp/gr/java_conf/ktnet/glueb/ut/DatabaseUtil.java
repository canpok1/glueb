package jp.gr.java_conf.ktnet.glueb.ut;

import lombok.experimental.UtilityClass;
import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

import java.io.File;
import javax.sql.DataSource;

/**
 * DB操作をまとめたクラスです.
 * @author tanabe
 *
 */
@UtilityClass
public class DatabaseUtil {
  
  /**
   * テスト用にDBを準備します.
   * テスト後はtearDownメソッドを呼び出してください.
   * @param proxy データソースのプロキシ.
   * @param recordXml テストデータを記載したxml.
   * @return IDatabaseTesterオブジェクト. tearDownメソッドを呼ぶときに必要になります.
   * @throws Exception エラー発生時.
   * @see DatabaseUtil#tearDown(IDatabaseTester)
   */
  public IDatabaseTester setup(
      TransactionAwareDataSourceProxy proxy,
      String recordXml
  ) throws Exception {
    IDatabaseTester databaseTester = new DataSourceDatabaseTester(proxy);
    File file = new File(recordXml);
    IDataSet xmlDataSet = new FlatXmlDataSetBuilder().build(file);
    databaseTester.setDataSet(xmlDataSet);
    databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
    databaseTester.onSetup();
    return databaseTester;
  }
  
  /**
   * DBを片付けます.
   * @param databaseTester setupメソッドの戻り値.
   * @throws Exception エラー発生時.
   * @see DatabaseUtil#setup(DataSource, String)
   */
  public void tearDown(IDatabaseTester databaseTester) throws Exception {
    if (databaseTester != null) {
      databaseTester.onTearDown();
    }
  }
}
