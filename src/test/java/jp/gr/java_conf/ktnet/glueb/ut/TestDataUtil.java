package jp.gr.java_conf.ktnet.glueb.ut;

import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;

/**
 * テストデータ向け操作をまとめたクラスです.
 * @author tanabe
 *
 */
public class TestDataUtil {

  /**
   * 日付文字列からDate型を生成します.
   * @param dateStr 日付文字列(2016/01/01 10:00:00).
   * @return 生成結果.
   */
  public static Date makeData(String dateStr) {
    try {
      return DateUtils.parseDateStrictly("2016/08/01 10:00:00", "yyyy/MM/dd hh:mm:ss");
    } catch (Exception e) {
      return null;
    }
  }
}
