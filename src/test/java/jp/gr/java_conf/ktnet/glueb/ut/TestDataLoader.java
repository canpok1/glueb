package jp.gr.java_conf.ktnet.glueb.ut;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

/**
 * JSONデータを読み取るクラスです.
 * @author tanabe
 */
@Slf4j
public class TestDataLoader {

  /**
   * ファイルをJSONオブジェクトとして読み込みます.
   * @param filePath ファイルパス
   * @return JSONとして読み込んだオブジェクト.
   */
  public Map<String, Object> loadFileAsJsonObject(String filePath) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      return mapper.readValue(new File(filePath), new TypeReference<Map<String, Object>>(){});
    } catch (IOException ex) {
      log.warn("ファイル読み込み時にエラー発生", ex);
      return null;
    }
  }
  
  /**
   * ファイルを読み込んで文字列として返します.
   * @param filePath ファイルパス.
   * @return 読み込んだ文字列.
   * @throws IOException 読み込みに失敗した場合.
   * @throws FileNotFoundException ファイルが見つからない場合.
   */
  public String loadFileAsString(String filePath) throws FileNotFoundException, IOException {
    try (FileReader fileReader = new FileReader(filePath);
        BufferedReader bufferedReader = new BufferedReader(fileReader)) {
      String line;
      String loaded = "";
      while ((line = bufferedReader.readLine()) != null) {
        if (!loaded.equals("")) {
          loaded += System.lineSeparator();
        }
        loaded += line;
      }
      return loaded;
    }
  }
}
