package jp.gr.java_conf.ktnet.glueb.it.model;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import jp.gr.java_conf.ktnet.glueb.model.ParamConvertService;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.IssueCreatedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.IssueUpdatedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.PullRequestApprovedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.PullRequestCommentCreatedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.PullRequestCreatedParam;
import jp.gr.java_conf.ktnet.glueb.ut.TestDataLoader;
import org.junit.Before;
import org.junit.Test;


public class ParamConvertServiceTest {

  /** テストデータ読み込み用オブジェクト. */
  private TestDataLoader testDataLoader;
  
  /** テスト対象. */
  private ParamConvertService target;
  
  @Before
  public void setup() {
    target = new ParamConvertService();
    testDataLoader = new TestDataLoader();
  }
  
  @Test
  public void convertToIssueCreatedParamでJSONをオブジェクトに変換できること() throws Exception {
    final String filePath = "./src/integration-test/resources/jp/gr/java_conf/ktnet/glueb/it/"
                          + "model/IssueCreated.json";
    String param = testDataLoader.loadFileAsString(filePath);
    
    IssueCreatedParam returnObj = target.convertToIssueCreatedParam(param);
    
    // IssueCreatedParamの確認
    assertThat(returnObj, is(not(nullValue())));
    assertThat(returnObj.getActor(), is(not(nullValue())));
    assertThat(returnObj.getIssue(), is(not(nullValue())));
    assertThat(returnObj.getRepository(), is(not(nullValue())));
    
    // Actorの確認
    assertThat(returnObj.getActor().getUsername(), is("emmap1"));
    assertThat(returnObj.getActor().getDisplayName(), is("Emma"));
    assertThat(
        returnObj.getActor().getLinks().getAvatar().getHref(),
        is("https://bitbucket-api-assetroot.s3.amazonaws.com/c/photos/2015/Feb/26/3613917261-0-emmap1-avatar_avatar.png"));
    
    // Issueの確認
    assertThat(returnObj.getIssue().getTitle(), is("Issue title"));
    assertThat(returnObj.getIssue().getContent(), is(not(nullValue())));
    assertThat(returnObj.getIssue().getContent(), hasKey("html"));
    assertThat(returnObj.getIssue().getContent(), hasKey("markup"));
    assertThat(returnObj.getIssue().getContent(), hasKey("raw"));
    assertThat(returnObj.getIssue().getLinks().getHtml().getHref(), is("https://api.bitbucket.org/issue_id"));
    
    // Repositoryの確認
    assertThat(returnObj.getRepository().getFullName(), is("team_name/repo_name"));
    assertThat(returnObj.getRepository().getName(), is("repo_name"));
  }
  
  @Test
  public void convertToIssueUpdatedParamでJSONをオブジェクトに変換できること() throws Exception {
    final String filePath = "./src/integration-test/resources/jp/gr/java_conf/ktnet/glueb/it/"
                          + "model/IssueUpdated.json";
    String param = testDataLoader.loadFileAsString(filePath);
    
    IssueUpdatedParam returnObj = target.convertToIssueUpdatedParam(param);
    
    // IssueCreatedParamの確認
    assertThat(returnObj, is(not(nullValue())));
    assertThat(returnObj.getActor(), is(not(nullValue())));
    assertThat(returnObj.getIssue(), is(not(nullValue())));
    assertThat(returnObj.getRepository(), is(not(nullValue())));
    assertThat(returnObj.getComment(), is(not(nullValue())));
    assertThat(returnObj.getChanges(), is(not(nullValue())));
    
    // Actorの確認
    assertThat(returnObj.getActor().getUsername(), is("emmap1"));
    assertThat(returnObj.getActor().getDisplayName(), is("Emma"));
    assertThat(
        returnObj.getActor().getLinks().getAvatar().getHref(),
        is("https://bitbucket-api-assetroot.s3.amazonaws.com/c/photos/2015/Feb/26/3613917261-0-emmap1-avatar_avatar.png"));
    
    // Issueの確認
    assertThat(returnObj.getIssue().getTitle(), is("Issue title"));
    assertThat(returnObj.getIssue().getContent(), is(not(nullValue())));
    assertThat(returnObj.getIssue().getContent(), hasKey("html"));
    assertThat(returnObj.getIssue().getContent(), hasKey("markup"));
    assertThat(returnObj.getIssue().getContent(), hasKey("raw"));
    assertThat(returnObj.getIssue().getLinks().getHtml().getHref(), is("https://api.bitbucket.org/issue_id"));
    
    // Repositoryの確認
    assertThat(returnObj.getRepository().getFullName(), is("team_name/repo_name"));
    assertThat(returnObj.getRepository().getName(), is("repo_name"));
    
    // Commentの確認
    assertThat(returnObj.getComment().getContent().getRaw(), is("Comment text"));
    assertThat(returnObj.getComment().getContent().getHtml(), is("<p>Comment text</p>"));
    assertThat(returnObj.getComment().getContent().getMarkup(), is("markdown"));
    
    // changesの確認
    assertThat(returnObj.getChanges().get("status").getNewValue(), is("on hold"));
    assertThat(returnObj.getChanges().get("status").getOldValue(), is("open"));
  }  
  
  @Test
  public void convertToPullRequestCreatedParamでJSONをオブジェクトに変換できること() throws Exception {
    final String filePath = "./src/integration-test/resources/jp/gr/java_conf/ktnet/glueb/it/"
                          + "model/PullRequestCreated.json";
    String param = testDataLoader.loadFileAsString(filePath);
    
    PullRequestCreatedParam returnObj = target.convertToPullRequestCreatedParam(param);
    
    // IssueCreatedParamの確認
    assertThat(returnObj, is(not(nullValue())));
    assertThat(returnObj.getActor(), is(not(nullValue())));
    assertThat(returnObj.getPullrequest(), is(not(nullValue())));
    assertThat(returnObj.getRepository(), is(not(nullValue())));
    
    // Actorの確認
    assertThat(returnObj.getActor().getUsername(), is("emmap1"));
    assertThat(returnObj.getActor().getDisplayName(), is("Emma"));
    assertThat(
        returnObj.getActor().getLinks().getAvatar().getHref(),
        is("https://bitbucket-api-assetroot.s3.amazonaws.com/c/photos/2015/Feb/26/3613917261-0-emmap1-avatar_avatar.png"));
    
    // Pullrequestの確認
    assertThat(returnObj.getPullrequest().getAuthor(), is(not(nullValue())));
    assertThat(returnObj.getPullrequest().getDescription(), is("Description of pull request"));
    assertThat(returnObj.getPullrequest().getTitle(), is("Title of pull request"));
    assertThat(returnObj.getPullrequest().getLinks().getHtml().getHref(), is("https://api.bitbucket.org/pullrequest_id"));
    
    // Repositoryの確認
    assertThat(returnObj.getRepository().getFullName(), is("team_name/repo_name"));
    assertThat(returnObj.getRepository().getName(), is("repo_name"));
  }
  
  @Test
  public void convertToPullRequestApprovedParamでJSONをオブジェクトに変換できること() throws Exception {
    final String filePath = "./src/integration-test/resources/jp/gr/java_conf/ktnet/glueb/it/"
                          + "model/PullRequestApproved.json";
    String param = testDataLoader.loadFileAsString(filePath);
    
    PullRequestApprovedParam returnObj = target.convertToPullRequestApprovedParam(param);
    
    // IssueCreatedParamの確認
    assertThat(returnObj, is(not(nullValue())));
    assertThat(returnObj.getActor(), is(not(nullValue())));
    assertThat(returnObj.getPullrequest(), is(not(nullValue())));
    assertThat(returnObj.getRepository(), is(not(nullValue())));
    assertThat(returnObj.getApproval(), is(not(nullValue())));
    
    // Actorの確認
    assertThat(returnObj.getActor().getUsername(), is("emmap1"));
    assertThat(returnObj.getActor().getDisplayName(), is("Emma"));
    assertThat(
        returnObj.getActor().getLinks().getAvatar().getHref(),
        is("https://bitbucket-api-assetroot.s3.amazonaws.com/c/photos/2015/Feb/26/3613917261-0-emmap1-avatar_avatar.png"));
    
    // Pullrequestの確認
    assertThat(returnObj.getPullrequest().getAuthor(), is(not(nullValue())));
    assertThat(returnObj.getPullrequest().getDescription(), is("Description of pull request"));
    assertThat(returnObj.getPullrequest().getTitle(), is("Title of pull request"));
    assertThat(returnObj.getPullrequest().getLinks().getHtml().getHref(), is("https://api.bitbucket.org/pullrequest_id"));
    
    // Repositoryの確認
    assertThat(returnObj.getRepository().getFullName(), is("team_name/repo_name"));
    assertThat(returnObj.getRepository().getName(), is("repo_name"));
    
    // Approvalの確認
    assertThat(returnObj.getApproval().getDate(), is("2015-04-06T16:34:59.195330+00:00"));
    assertThat(returnObj.getApproval().getUser().getUsername(), is("emmap1"));
  }
  
  @Test
  public void convertToPullRequestCommentCreatedParamでJSONをオブジェクトに変換できること() throws Exception {
    final String filePath = "./src/integration-test/resources/jp/gr/java_conf/ktnet/glueb/it/"
                          + "model/PullRequestCommentCreated.json";
    String param = testDataLoader.loadFileAsString(filePath);
    
    PullRequestCommentCreatedParam returnObj
        = target.convertToPullRequestCommentCreatedParam(param);
    
    // IssueCreatedParamの確認
    assertThat(returnObj, is(not(nullValue())));
    assertThat(returnObj.getActor(), is(not(nullValue())));
    assertThat(returnObj.getPullrequest(), is(not(nullValue())));
    assertThat(returnObj.getRepository(), is(not(nullValue())));
    assertThat(returnObj.getComment(), is(not(nullValue())));
    
    // Actorの確認
    assertThat(returnObj.getActor().getUsername(), is("emmap1"));
    assertThat(returnObj.getActor().getDisplayName(), is("Emma"));
    assertThat(
        returnObj.getActor().getLinks().getAvatar().getHref(),
        is("https://bitbucket-api-assetroot.s3.amazonaws.com/c/photos/2015/Feb/26/3613917261-0-emmap1-avatar_avatar.png"));
    
    // Pullrequestの確認
    assertThat(returnObj.getPullrequest().getAuthor(), is(not(nullValue())));
    assertThat(returnObj.getPullrequest().getDescription(), is("Description of pull request"));
    assertThat(returnObj.getPullrequest().getTitle(), is("Title of pull request"));
    assertThat(returnObj.getPullrequest().getLinks().getHtml().getHref(), is("https://api.bitbucket.org/pullrequest_id"));
    
    // Repositoryの確認
    assertThat(returnObj.getRepository().getFullName(), is("team_name/repo_name"));
    assertThat(returnObj.getRepository().getName(), is("repo_name"));
    
    // Commentの確認
    assertThat(returnObj.getComment().getContent().getRaw(), is("Comment text"));
    assertThat(returnObj.getComment().getContent().getHtml(), is("<p>Comment text</p>"));
    assertThat(returnObj.getComment().getContent().getMarkup(), is("markdown"));
  }
}

