package jp.gr.java_conf.ktnet.glueb.it;

import jp.gr.java_conf.ktnet.glueb.Application;
import jp.gr.java_conf.ktnet.glueb.ut.DatabaseUtil;
import org.dbunit.IDatabaseTester;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.ITable;
import org.junit.ClassRule;
import org.junit.Rule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.sql.Statement;

import javax.sql.DataSource;


/**
 * 結合テストの基底クラスです.
 * Spring関連の設定やDBへのデータ投入などの処理をまとめてます.
 * @author tanabe
 */

@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@Transactional
public class IntegrationTestBase {
  @ClassRule
  public static final SpringClassRule springClassRule = new SpringClassRule();
  
  @Rule
  public final SpringMethodRule springMethodRule = new SpringMethodRule();
  
  /**
   * アプリケーション情報.
   */
  @Autowired
  private WebApplicationContext webApplicationContext;
  
  /**
   * DB接続情報.
   */
  @Autowired
  private DataSource dataSource;
  
  /**
   * MockMvcオブジェクト.
   * HTTPリクエスト関連の処理や検証を行います.
   */
  protected MockMvc mvc;
  
  /**
   * DBへのデータ投入を行うオブジェクト.
   */
  private IDatabaseTester databaseTester;
  
  /**
   * DBへの接続オブジェクト.
   */
  private IDatabaseConnection databaseConnection;
  
  /**
   * MockMvcオブジェクトの初期設定を行います.
   * @param webApplicationContext Webアプリ情報.
   */
  protected void setupMvc() {
    mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
  }
  
  /**
   * テスト用にDBへとデータを投入します.
   * このメソッドを呼んだ場合、必ずtearDownDatabaseメソッドを呼び出してください.
   * @param recordXml データを記載したxmlファイルのパス.
   * @throws Exception 例外発生時.
   * @see IntegrationTestBase#tearDownDatabase()
   */
  protected void setupDatabase(String recordXml) throws Exception {
    TransactionAwareDataSourceProxy proxy = new TransactionAwareDataSourceProxy(dataSource);
    databaseTester = DatabaseUtil.setup(proxy, recordXml);
    databaseConnection = new DatabaseConnection(proxy.getConnection());
  }
  
  /**
   * DBの後処理を行います.
   * setupDatabaseを呼んだ場合、必ずこのメソッドを呼び出してください.
   * @throws Exception 例外発生時.
   * @see IntegrationTestBase#setupDatabase(String)
   */
  protected void tearDownDatabase() throws Exception {
    DatabaseUtil.tearDown(databaseTester);
    if (databaseConnection != null) {
      databaseConnection.close();
    }
  }
  
  /**
   * DBの指定テーブルの値を取得します.
   * @param tableName テーブル名.
   * @return テーブルの値.
   * @throws Exception 例外発生時.
   */
  protected ITable fetchTable(String tableName) throws Exception {
    return databaseConnection.createDataSet().getTable(tableName);
  }
}
