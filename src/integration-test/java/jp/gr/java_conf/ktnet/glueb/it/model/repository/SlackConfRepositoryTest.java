package jp.gr.java_conf.ktnet.glueb.it.model.repository;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import jp.gr.java_conf.ktnet.glueb.it.IntegrationTestBase;
import jp.gr.java_conf.ktnet.glueb.model.entity.db.SlackConf;
import jp.gr.java_conf.ktnet.glueb.model.repository.SlackConfRepository;
import jp.gr.java_conf.ktnet.glueb.ut.TestDataUtil;
import lombok.AllArgsConstructor;
import lombok.Data;

import org.junit.experimental.runners.Enclosed;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RunWith(Enclosed.class)
public class SlackConfRepositoryTest {
  
  private static final String EMPTY_ALL
      = "./src/integration-test/resources/"
      + "jp/gr/java_conf/ktnet/glueb/it/model/repository/EmptyAll.xml";
  private static final String MANY_SLACK_CONFS
      = "./src/integration-test/resources/"
      + "jp/gr/java_conf/ktnet/glueb/it/model/repository/slackconf/ManySlackConfs.xml";

  @RunWith(Theories.class)
  public static class findAllを呼んだ時 extends IntegrationTestBase {
    
    @Autowired
    private SlackConfRepository repository;
    
    @Data
    @AllArgsConstructor
    public static class Fixture {
      private String testDataFile;
      private List<SlackConf> records;
    }
    
    @DataPoints
    public static Fixture[] fixtures = {
        new Fixture(EMPTY_ALL, Arrays.asList()),
        new Fixture(MANY_SLACK_CONFS, Arrays.asList(
            new SlackConf("1", "apitoken1", "channel1", "emoji1", "user1", new Date(), new Date()),
            new SlackConf("2", "apitoken2", "channel2", null, null, new Date(), new Date())
            ))
    };
    
    @Theory
    public void 戻り値が想定通り(Fixture fixture) throws Exception {
      this.setupMvc();
      this.setupDatabase(fixture.testDataFile);
      assertThat(repository.findAll(), hasSize(fixture.records.size()));
    }
  }
  
  @RunWith(Theories.class)
  public static class findByIdを呼んだ時 extends IntegrationTestBase {
    
    @Autowired
    private SlackConfRepository repository;
    
    @Data
    @AllArgsConstructor
    public static class Fixture {
      private String testDataFile;
      private String id;
      private SlackConf record;
    }
    
    @DataPoints
    public static Fixture[] fixtures = {
        new Fixture(MANY_SLACK_CONFS, "0", null),
        new Fixture(
            MANY_SLACK_CONFS,
            "1",
            new SlackConf(
                "1",
                "apitoken1",
                "channel1",
                "emoji1",
                "user1",
                TestDataUtil.makeData("2016/08/01 10:00:00"),
                TestDataUtil.makeData("2016/08/01 10:00:00")
            )
        ),
        new Fixture(
            MANY_SLACK_CONFS,
            "2",
            new SlackConf(
                "2",
                "apitoken2",
                "channel2",
                null,
                null,
                TestDataUtil.makeData("2016/08/01 10:00:00"),
                TestDataUtil.makeData("2016/08/01 10:00:00")
            )
        ),
    };
    
    @Theory
    public void 戻り値が想定通り(Fixture fixture) throws Exception {
      this.setupDatabase(fixture.testDataFile);
      
      // DBに登録されてるかを検証
      SlackConf returnedValue = repository.findById(fixture.getId());
      if (fixture.getRecord() == null) {
        assertThat(returnedValue, is(nullValue()));
      } else {
        assertThat(returnedValue, is(samePropertyValuesAs(fixture.getRecord())));
      }
    }
  }
}
