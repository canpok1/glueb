package jp.gr.java_conf.ktnet.glueb.it.model.repository;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import jp.gr.java_conf.ktnet.glueb.it.IntegrationTestBase;
import jp.gr.java_conf.ktnet.glueb.model.entity.db.Route;
import jp.gr.java_conf.ktnet.glueb.model.repository.RouteRepository;
import lombok.AllArgsConstructor;
import lombok.Data;

import org.junit.After;
import org.junit.experimental.runners.Enclosed;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RunWith(Enclosed.class)
public class RouteRepositoryTest {
  
  private static final String EMPTY_ALL
      = "./src/integration-test/resources/"
      + "jp/gr/java_conf/ktnet/glueb/it/model/repository/EmptyAll.xml";
  private static final String MANY_ROUTES
      = "./src/integration-test/resources/"
      + "jp/gr/java_conf/ktnet/glueb/it/model/repository/route/ManyRoutes.xml";

  @RunWith(Theories.class)
  public static class findAllを呼んだ時 extends IntegrationTestBase {
    
    @Autowired
    private RouteRepository repository;
    
    @Data
    @AllArgsConstructor
    public static class Fixture {
      private String testDataFile;
      private List<Route> returnedValues;
    }
    
    @DataPoints
    public static Fixture[] fixtures = {
        new Fixture(EMPTY_ALL, Arrays.asList()),
        new Fixture(MANY_ROUTES, Arrays.asList(
            new Route("0", 0, "0", 0, new Date(), new Date()),
            new Route("1", 1, "0", 0, new Date(), new Date()),
            new Route("1", 1, "1", 1, new Date(), new Date()),
            new Route("2", 0, "0", 0, new Date(), new Date()),
            new Route("2", 0, "1", 1, new Date(), new Date()),
            new Route("2", 0, "2", 2, new Date(), new Date()),
            new Route("3", 1, "0", 0, new Date(), new Date()),
            new Route("3", 1, "1", 1, new Date(), new Date()),
            new Route("3", 1, "2", 2, new Date(), new Date()),
            new Route("3", 1, "3", 3, new Date(), new Date())
            ))
    };
    
    @Theory
    public void 戻り値が想定通り(Fixture fixture) throws Exception {
      this.setupDatabase(fixture.testDataFile);
      assertThat(repository.findAll(), hasSize(fixture.returnedValues.size()));
    }
    
    @After
    public void tearDown() throws Exception {
      this.tearDownDatabase();
    }
  }
  
  @RunWith(Theories.class)
  public static class findByEntranceを呼んだ時 extends IntegrationTestBase {
    
    @Autowired
    private RouteRepository repository;
    
    @Data
    @AllArgsConstructor
    public static class Fixture {
      private String testDataFile;
      private String entranceId;
      private int entranceType;
      private List<Route> returnedValues;
    }
    
    @DataPoints
    public static Fixture[] fixtures = {
        new Fixture(EMPTY_ALL,"1", 0, Arrays.asList()),
        
        // 該当レコードなし
        new Fixture(MANY_ROUTES, "0", 1, Arrays.asList()),
        // 該当レコードが1つ
        new Fixture(MANY_ROUTES, "0", 0, Arrays.asList(
            new Route("0", 0, "0", 0, new Date(), new Date())
            )),
        // 該当レコードが複数
        new Fixture(MANY_ROUTES, "1", 1, Arrays.asList(
            new Route("1", 1, "0", 0, new Date(), new Date()),
            new Route("1", 1, "1", 1, new Date(), new Date())
            )),
    };
    
    @Theory
    public void 戻り値が想定通り(Fixture fixture) throws Exception {
      this.setupDatabase(fixture.testDataFile);
      List<Route> allRecords = repository.findAll();
      assertThat(
          repository.findByEntrance(fixture.entranceId, fixture.entranceType),
          hasSize(fixture.returnedValues.size()));
    }
    
    @After
    public void tearDown() throws Exception {
      this.tearDownDatabase();
    }
  }
}
