package jp.gr.java_conf.ktnet.glueb.config;

import lombok.Getter;

import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * 環境設定を取得するためのクラスです.
 * @author tanabe
 *
 */
@Component
@Getter
public class EnvironmentValues {

  private static final String slackApiTokenKey = "SLACK_API_TOKEN";
  
  // herokuで設定される接続値
  private static final String databaseUrlKey = "DATABASE_URL";
  // shippableで設定される接続値
  private static final String databaseUrlNonSslKey = "DATABASE_URL_NON_SSL";
  
  // DBのスキーマ名
  private static final String schemaName = "glueb";
  
  private final String databaseJdbcUrl;
  private final String databaseUsername;
  private final String databasePassword;
  
  /**
   * コンストラクタ.
   * envオブジェクトから値を取得して保持します.
   * @throws URISyntaxException URIの変換に失敗した場合.
   */
  public EnvironmentValues() throws URISyntaxException {
    String databaseUrl = System.getenv(databaseUrlKey);
    String param = "?currentSchema=" + schemaName;
    
    if (databaseUrl == null || databaseUrl.equals("")) {
      databaseUrl = System.getenv(databaseUrlNonSslKey);
    } else {
      param += "&sslmode=require";
    }
    
    if (databaseUrl != null) {
      URI dbUri = new URI(databaseUrl);
      
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("jdbc:postgresql://");
      stringBuilder.append(dbUri.getHost());
      stringBuilder.append(':');
      stringBuilder.append(dbUri.getPort());
      stringBuilder.append(dbUri.getPath());
      stringBuilder.append(param);
      
      databaseJdbcUrl = stringBuilder.toString();
      
      String[] splited = dbUri.getUserInfo().split(":");
      databaseUsername = (splited == null || splited.length < 1) ? "" : splited[0];
      databasePassword = (splited == null || splited.length < 2) ? "" : splited[1];
    } else {
      databaseJdbcUrl = null;
      databaseUsername = null;
      databasePassword = null;
    }
  }
  
  /**
   * SlackのAPIトークンを取得します.
   * @return SlackのAPIトークン.
   */
  public String getSlackApiToken() {
    return System.getenv(slackApiTokenKey);
  }
}
