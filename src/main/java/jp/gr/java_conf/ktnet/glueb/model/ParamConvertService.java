package jp.gr.java_conf.ktnet.glueb.model;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.IssueCreatedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.IssueUpdatedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.PullRequestApprovedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.PullRequestCommentCreatedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.PullRequestCreatedParam;

import org.springframework.stereotype.Service;

import java.io.IOException;


/**
 * パラメータをオブジェクトに変換するクラスです.
 * @author tanabe
 */
@Service
public class ParamConvertService {

  /**
   * 課題作成時のパラメータをオブジェクトに変換します.
   * @param param Webhookで受信したパラメータ.
   * @return 変換結果.
   * @throws IOException 変換できなかった場合.
   * @throws JsonMappingException 変換できなかった場合.
   * @throws JsonParseException 変換できなかった場合.
   */
  public IssueCreatedParam convertToIssueCreatedParam(
      String param
  ) throws JsonParseException, JsonMappingException, IOException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(param, IssueCreatedParam.class);
  }
  
  /**
   * 課題更新時のパラメータをオブジェクトに変換します.
   * @param param Webhookで受信したパラメータ.
   * @return 変換結果.
   * @throws IOException 変換できなかった場合.
   * @throws JsonMappingException 変換できなかった場合.
   * @throws JsonParseException 変換できなかった場合.
   */
  public IssueUpdatedParam convertToIssueUpdatedParam(
      String param
  ) throws JsonParseException, JsonMappingException, IOException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(param, IssueUpdatedParam.class);
  }

  /**
   * プルリクエスト作成時のパラメータをオブジェクトに変換します.
   * @param param Webhookで受信したパラメータ.
   * @return 変換結果.
   * @throws IOException 変換できなかった場合.
   * @throws JsonMappingException 変換できなかった場合.
   * @throws JsonParseException 変換できなかった場合.
   */
  public PullRequestCreatedParam convertToPullRequestCreatedParam(
      String param
  ) throws JsonParseException, JsonMappingException, IOException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(param, PullRequestCreatedParam.class);
  }

  /**
   * プルリクエスト承認時のパラメータをオブジェクトに変換します.
   * @param param Webhookで受信したパラメータ.
   * @return 変換結果.
   * @throws IOException 変換できなかった場合.
   * @throws JsonMappingException 変換できなかった場合.
   * @throws JsonParseException 変換できなかった場合.
   */
  public PullRequestApprovedParam convertToPullRequestApprovedParam(
      String param
  ) throws JsonParseException, JsonMappingException, IOException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(param, PullRequestApprovedParam.class);
  }

  /**
   * プルリクエストコメント作成時のパラメータをオブジェクトに変換します.
   * @param param Webhookで受信したパラメータ.
   * @return 変換結果.
   * @throws IOException 変換できなかった場合.
   * @throws JsonMappingException 変換できなかった場合.
   * @throws JsonParseException 変換できなかった場合.
   */
  public PullRequestCommentCreatedParam convertToPullRequestCommentCreatedParam(
      String param
  ) throws JsonParseException, JsonMappingException, IOException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(param, PullRequestCommentCreatedParam.class);
  }

}
