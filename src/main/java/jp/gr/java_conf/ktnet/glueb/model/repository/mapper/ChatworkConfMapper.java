package jp.gr.java_conf.ktnet.glueb.model.repository.mapper;

import jp.gr.java_conf.ktnet.glueb.model.entity.db.ChatworkConf;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * chatwork_confテーブルからのレコード取得方法を定義するインターフェースです.
 * @author tanabe
 */
public interface ChatworkConfMapper {
  /**
   * 全レコードを取得します.
   * @return 全レコード.
   */
  @Select(
      "SELECT"
      + " id,"
      + " api_token,"
      + " room_id,"
      + " created,"
      + " updated "
      + "FROM"
      + " glueb.chatwork_conf")
  public List<ChatworkConf> findAll();
  
  /**
   * ID指定でレコードを取得します.
   * @return レコード.
   */
  @Select(
      "SELECT"
      + " id,"
      + " api_token,"
      + " room_id,"
      + " created,"
      + " updated "
      + "FROM"
      + " glueb.chatwork_conf "
      + "WHERE"
      + " id = #{id}")
  public ChatworkConf findById(
      @Param("id") final String id);

}
