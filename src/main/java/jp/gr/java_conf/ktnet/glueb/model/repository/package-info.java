/**
 * Repositoryアノテーションを付与したクラス向けパッケージです.
 * @author tanabe
 */
package jp.gr.java_conf.ktnet.glueb.model.repository;