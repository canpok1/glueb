package jp.gr.java_conf.ktnet.glueb.model.repository;

import jp.gr.java_conf.ktnet.glueb.model.entity.db.SlackConf;
import jp.gr.java_conf.ktnet.glueb.model.repository.mapper.SlackConfMapper;
import lombok.NonNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * slack_confテーブルの情報を操作するクラスです.
 * @author tanabe
 */
@Repository
public class SlackConfRepository {

  @Autowired
  private SlackConfMapper mapper;
  
  /**
   * 全レコードを取得します.
   * @return 全レコード.
   */
  public List<SlackConf> findAll() {
    return mapper.findAll();
  }
  
  /**
   * 指定したIDのレコードを取得します.
   * @param id ID.
   * @return レコード.
   */
  public SlackConf findById(
      @NonNull final String id
  ) {
    return mapper.findById(id);
  }
}
