package jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class IssueParam {
  private String title;
  private Map<String, String> content;
  private LinksParam links;
}
