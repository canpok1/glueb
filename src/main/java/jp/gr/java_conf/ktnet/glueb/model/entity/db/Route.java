package jp.gr.java_conf.ktnet.glueb.model.entity.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * サービス間の情報の流し方を保持するクラスです.
 * @author tanabe
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Route {
  private String entranceId;
  private Integer entranceType;
  private String exitId;
  private Integer exitType;
  private Date created;
  private Date updated;
}
