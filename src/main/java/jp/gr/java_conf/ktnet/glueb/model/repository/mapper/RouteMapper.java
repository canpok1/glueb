package jp.gr.java_conf.ktnet.glueb.model.repository.mapper;

import jp.gr.java_conf.ktnet.glueb.model.entity.db.Route;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * routeテーブルからのレコード取得方法を定義するインターフェースです.
 * @author tanabe
 */
public interface RouteMapper {
  /**
   * 全レコードを取得します.
   * @return 全レコード.
   */
  @Select(
      "SELECT"
      + " entrance_id,"
      + " entrance_type,"
      + " exit_id,"
      + " exit_type,"
      + " created,"
      + " updated "
      + "FROM"
      + " glueb.route")
  public List<Route> findAll();
  
  /**
   * 指定の入口情報に一致するレコードを取得します.
   * @return レコード.
   */
  @Select(
      "SELECT"
      + " entrance_id,"
      + " entrance_type,"
      + " exit_id,"
      + " exit_type,"
      + " created,"
      + " updated "
      + "FROM"
      + " glueb.route "
      + "WHERE"
      + " entrance_id = #{entranceId}"
      + " AND entrance_type = #{entranceType}")
  public List<Route> findByEntrance(
      @Param("entranceId") final String entranceId,
      @Param("entranceType") final int entranceType);
}
