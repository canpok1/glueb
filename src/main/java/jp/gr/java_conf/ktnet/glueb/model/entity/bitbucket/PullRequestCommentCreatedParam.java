package jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PullRequestCommentCreatedParam {
  private OwnerParam actor;
  private RepositoryParam repository;
  private PullRequestParam pullrequest;
  private CommentParam comment;
}
