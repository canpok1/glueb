package jp.gr.java_conf.ktnet.glueb.controller.api;

import jp.gr.java_conf.ktnet.glueb.model.RoutingService;
import jp.gr.java_conf.ktnet.glueb.model.entity.ApiResponseTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * Bitbucket向けのAPIを定義するクラスです.
 * @author tanabe
 *
 */
@RestController
@RequestMapping("/api/bitbucket")
@Slf4j
public class BitbucketApiController {

  @Autowired
  private RoutingService service;
  
  /**
   * Webフックの通知を受け取ります.
   * @param eventKey イベントキー.
   * @param param パラメータ.
   * @param response レスポンス.
   * @return レスポンスのボディ.
   * @throws Exception 例外発生.
   */
  @RequestMapping(value = "/webhook", method = RequestMethod.POST)
  public ApiResponseTemplate<String> receiveWebhook(
      @RequestParam("id")           final String entranceId,
      @RequestHeader("X-Event-Key") final String eventKey,
      @RequestBody                  final String param,
                                    final HttpServletResponse response
  ) throws Exception {
    log.info("BitbucketのWebフック受信[id={}][{}][{}]", entranceId, eventKey, param);
    
    service.processForBitbucket(entranceId, eventKey, param);
    
    response.addHeader("Access-Control-Allow-Origin", "*" );
    return new ApiResponseTemplate<String>("テスト用レスポンス", "レスポンスボディ");
  }
}
