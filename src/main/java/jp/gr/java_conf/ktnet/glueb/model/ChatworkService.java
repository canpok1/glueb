package jp.gr.java_conf.ktnet.glueb.model;

import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.IssueCreatedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.IssueUpdatedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.PullRequestApprovedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.PullRequestCommentCreatedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.PullRequestCreatedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.db.ChatworkConf;
import jp.gr.java_conf.ktnet.glueb.model.repository.ChatworkConfRepository;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;

/**
 * Chatwork関連処理を行うクラスです.
 * @author tanabe
 */
@Service
@Slf4j
public class ChatworkService {

  private static final String CHATWORK_API_BASE_URL = "https://api.chatwork.com/v1";
  
  @Autowired
  private ChatworkConfRepository repository;
  
  /**
   * Slackにメッセージを投稿します.
   * @param confId Chatwork設定ID.
   * @param message メッセージ.
   * @throws URISyntaxException メッセージ投稿に失敗した場合.
   */
  private void postMessage(
      @NonNull String confId,
      @NonNull String message
  ) throws URISyntaxException {
    
    ChatworkConf conf = repository.findById(confId);
    if (conf == null) {
      log.warn("Chatwork設定が見つかりません[id={}]", confId);
      return;
    }
    
    log.info("Send message to chatwork");
    
    String url = CHATWORK_API_BASE_URL + "/rooms/" + conf.getRoomId() + "/messages";
    
    HttpHeaders header = new HttpHeaders();
    header.add("X-ChatWorkToken", conf.getApiToken());
    
    MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
    body.add("body", message);
    
    HttpEntity<?> requestEntity = new HttpEntity<Object>(body, header);
    
    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity<String> response
        = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
    log.info("Response[" + response + "]");
  }

  /**
   * Chatworkにメッセージを投稿します.
   * @param confId Chatwork設定ID.
   * @param message メッセージ.
   * @throws URISyntaxException 投稿に失敗した場合. 
   */
  public void notify(String confId, String message) throws URISyntaxException {
    postMessage(confId, message);
  }

  /**
   * Chatworkにメッセージを投稿します.
   * @param confId Chatwork設定ID.
   * @param param パラメータ..
   * @throws URISyntaxException 投稿に失敗した場合. 
   */
  public void notify(
      @NonNull String confId,
      @NonNull IssueCreatedParam param
  ) throws URISyntaxException {
    // TODO メッセージ組み立て
    notify(confId, "課題が新規作成されました");
  }

  /**
   * Chatworkにメッセージを投稿します.
   * @param confId Chatwork設定ID.
   * @param param パラメータ..
   * @throws URISyntaxException 投稿に失敗した場合. 
   */
  public void notify(
      @NonNull String confId,
      @NonNull IssueUpdatedParam param
  ) throws URISyntaxException {
    // TODO メッセージ組み立て
    notify(confId, "課題が更新されました");
  }

  /**
   * Chatworkにメッセージを投稿します.
   * @param confId Chatwork設定ID.
   * @param param パラメータ..
   * @throws URISyntaxException 投稿に失敗した場合. 
   */
  public void notify(
      @NonNull String confId,
      @NonNull PullRequestCreatedParam param
  ) throws URISyntaxException {
    // TODO メッセージ組み立て
    notify(confId, "プルリクエストが新規作成されました");
  }

  /**
   * Chatworkにメッセージを投稿します.
   * @param confId Chatwork設定ID.
   * @param param パラメータ..
   * @throws URISyntaxException 投稿に失敗した場合. 
   */
  public void notify(
      @NonNull String confId,
      @NonNull PullRequestApprovedParam param
  ) throws URISyntaxException {
    // TODO メッセージ組み立て
    notify(confId, "プルリクエストが承認されました");
  }

  /**
   * Chatworkにメッセージを投稿します.
   * @param confId Chatwork設定ID.
   * @param param パラメータ..
   * @throws URISyntaxException 投稿に失敗した場合. 
   */
  public void notify(
      @NonNull String confId,
      @NonNull PullRequestCommentCreatedParam param
  ) throws URISyntaxException {
    // TODO メッセージ組み立て
    notify(confId, "プルリクエストにコメントが追加されました");
  }
  
  
}
