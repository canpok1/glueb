package jp.gr.java_conf.ktnet.glueb.model.entity.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Chatworkの設定を保持するクラスです.
 * @author tanabe
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatworkConf {
  private String id;
  private String apiToken;
  private String roomId;
}
