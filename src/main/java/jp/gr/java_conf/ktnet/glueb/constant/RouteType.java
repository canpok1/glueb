package jp.gr.java_conf.ktnet.glueb.constant;

import lombok.Getter;

/**
 * 経路の種別を表す列挙です.
 * @author tanabe
 */
@Getter
public enum RouteType {
  
  UNKNOWN(-1),
  BITBUCKET(0),
  TRELLO(1),
  SLACK(2),
  CHATWORK(3),
  ;
  
  private int typeValue;
  
  private RouteType(int typeValue) {
    this.typeValue = typeValue;
  }
  
  /**
   * 数値をEnumに変換します.
   * @param typeValue 数値.
   * @return Enum.
   */
  public static RouteType toEnum(int typeValue) {
    for (RouteType routeType : RouteType.values()) {
      if (routeType.getTypeValue() == typeValue) {
        return routeType;
      }
    }
    return RouteType.UNKNOWN;
  }
}
