package jp.gr.java_conf.ktnet.glueb.model.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * API用のレスポンス向けクラスです.
 * @author tanabe
 *
 * @param <T> レスポンスに含める情報の型.
 */
@Data
@RequiredArgsConstructor
public class ApiResponseTemplate<T> {
  private final String message;
  private final T body;
}
