package jp.gr.java_conf.ktnet.glueb.config;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;


/**
 * DB関連の設定クラスです.
 * @author tanabe
 * 
 */
@Configuration
@EnableTransactionManagement
@MapperScan(basePackages = "jp.gr.java_conf.ktnet.glueb.model.repository.mapper")
@Slf4j
public class DatabaseConfig {

  @Autowired
  private EnvironmentValues environmentValues;

  /**
   * DataSourseを返します.
   * @return DataSource.
   */
  @Bean
  public DataSource dataSource() {
    final HikariDataSource hds = new HikariDataSource();
    
    String jdbcUrl = environmentValues.getDatabaseJdbcUrl();
    String userName = environmentValues.getDatabaseUsername();
    String password = environmentValues.getDatabasePassword();
    
    // パスワードの一文字目以降をすべて*に置き換える
    String maskedPassword = "";
    if (password != null && password.length() > 0) {
      maskedPassword = password.substring(0, 1);
      while (maskedPassword.length() < password.length()) {
        maskedPassword += "*";
      }
    }

    log.info("Database JdbcUrl: {}", jdbcUrl);
    log.info("Database Username: {}", userName);
    log.info("Database Password: {}", maskedPassword);
    
    hds.setJdbcUrl(jdbcUrl);
    hds.setUsername(userName);
    hds.setPassword(password);
    return hds;
  }

  /**
   * DataSourceTransactionManagerを返します.
   * @return DataSourceTransactionManager.
   */
  @Bean
  public DataSourceTransactionManager transactionManager() {
    return new DataSourceTransactionManager(dataSource());
  }
  
  /**
   * SqlSessionFactoryを返します.
   * @param dataSource DataSource.
   * @return SqlSessionFactory.
   * @throws Exception 例外が発生した場合.
   */
  @Bean
  public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
    final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
    sessionFactory.setDataSource(dataSource());
    sessionFactory.setConfigLocation(new ClassPathResource("config/mybatis.xml"));
    return sessionFactory.getObject();
  }
}