package jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * BitbucketのWebhookイベント種別.
 * @author tanabe
 */
@Getter
@RequiredArgsConstructor
public enum WebhookEventType {
  /** プルリクエストが作成された. */
  PULL_REQUEST_CREATED("pullrequest:created", "プルリクエスト作成"),
  /** プルリクエストが承認された. */
  PULL_REQUEST_APPROVED("pullrequest:approved", "プルリクエスト承認"),
  /** プルリクエストのコメントが作成された. */
  PULL_REQUEST_COMMENT_CREATED("pullrequest:comment_created", "プルリクエストコメント作成"),
  /** 課題が作成された. */
  ISSUE_CREATED("issue:created", "課題作成"),
  /** 課題が更新された. */
  ISSUE_UPDATED("issue:updated", "課題更新"),
  /** 未知のイベント. */
  UNKNOWN(null, "未対応のイベント")
  ;
  
  private final String eventKey;
  private final String eventName;
  
  /**
   * イベントキーに対応するイベント種別を取得します.
   * @param eventKey イベントキー.
   * @return イベント種別.
   */
  public static WebhookEventType toEnum(@NonNull String eventKey) {
    for (WebhookEventType eventType : WebhookEventType.values()) {
      if (eventKey.equals(eventType.eventKey)) {
        return eventType;
      }
    }
    return UNKNOWN;
  }
}