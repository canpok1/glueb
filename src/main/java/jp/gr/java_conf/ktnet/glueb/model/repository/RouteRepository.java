package jp.gr.java_conf.ktnet.glueb.model.repository;

import jp.gr.java_conf.ktnet.glueb.model.entity.db.Route;
import jp.gr.java_conf.ktnet.glueb.model.repository.mapper.RouteMapper;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Routeテーブルの情報を操作するクラスです.
 * @author tanabe
 */
@Repository
public class RouteRepository {

  @Autowired
  private RouteMapper mapper;
  
  /**
   * 全レコードを取得します.
   * @return 全レコード.
   */
  public List<Route> findAll() {
    return mapper.findAll();
  }

  /**
   * 指定した入口側情報に一致するレコードを取得します.
   * @param entranceId 入口ID.
   * @param entranceType 入口種別.
   * @return レコード.
   */
  public List<Route> findByEntrance(
      @NonNull String entranceId,
               int    entranceType
  ) {
    return mapper.findByEntrance(entranceId, entranceType);
  }
}
