package jp.gr.java_conf.ktnet.glueb.model.entity.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Slackの設定を保持するクラスです.
 * @author tanabe
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SlackConf {
  private String id;
  private String apiToken;
  private String channelName;
  private String iconEmoji;
  private String username;
  private Date created;
  private Date updated;
}
