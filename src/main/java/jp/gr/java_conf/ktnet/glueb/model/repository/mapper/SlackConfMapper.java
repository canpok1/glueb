package jp.gr.java_conf.ktnet.glueb.model.repository.mapper;

import jp.gr.java_conf.ktnet.glueb.model.entity.db.SlackConf;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * slack_confテーブルからのレコード取得方法を定義するインターフェースです.
 * @author tanabe
 */
public interface SlackConfMapper {
  /**
   * 全レコードを取得します.
   * @return 全レコード.
   */
  @Select(
      "SELECT"
      + " id,"
      + " api_token,"
      + " channel_name,"
      + " icon_emoji,"
      + " username,"
      + " created,"
      + " updated "
      + "FROM"
      + " glueb.slack_conf")
  public List<SlackConf> findAll();
  
  /**
   * ID指定でレコードを取得します.
   * @return レコード.
   */
  @Select(
      "SELECT"
      + " id,"
      + " api_token,"
      + " channel_name,"
      + " icon_emoji,"
      + " username,"
      + " created,"
      + " updated "
      + "FROM"
      + " glueb.slack_conf "
      + "WHERE"
      + " id = #{id}")
  public SlackConf findById(
      @Param("id") final String id);
}
