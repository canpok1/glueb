package jp.gr.java_conf.ktnet.glueb.model;

import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.IssueCreatedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.IssueUpdatedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.PullRequestApprovedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.PullRequestCommentCreatedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.PullRequestCreatedParam;
import jp.gr.java_conf.ktnet.glueb.model.entity.db.SlackConf;
import jp.gr.java_conf.ktnet.glueb.model.repository.SlackConfRepository;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Slack関連処理を行うクラスです.
 * @author tanabe
 */
@Service
@Slf4j
public class SlackService {

  private static final String SLACK_URL = "https://slack.com/api/chat.postMessage";
  
  @Autowired
  private SlackConfRepository slackConfRepository;
  
  /**
   * Slackにメッセージを投稿します.
   * @param slackConfId Slack設定ID.
   * @param message メッセージ.
   * @throws URISyntaxException メッセージ投稿に失敗した場合.
   */
  private void postMessage(
      @NonNull final String slackConfId,
      @NonNull String message
  ) throws URISyntaxException {
    SlackConf slackConf = slackConfRepository.findById(slackConfId);
    if (slackConf == null) {
      log.warn("Slack設定が見つかりません[id={}]", slackConfId);
      return;
    }
    
    log.info("Send message to slack channel[" + slackConf.getChannelName() + "][" + message + "]");
    
    MultiValueMap<String, Object> requestMap = new LinkedMultiValueMap<String, Object>();
    requestMap.add("token", slackConf.getApiToken());
    requestMap.add("channel", slackConf.getChannelName());
    requestMap.add("text", message);
    
    if (slackConf.getUsername() != null) {
      requestMap.add("username", slackConf.getUsername());
    }
    if (slackConf.getIconEmoji() != null) {
      requestMap.add("icon_emoji", slackConf.getIconEmoji());
    }
    
    RestTemplate restTemplate = new RestTemplate();
    String response = restTemplate.postForObject(new URI(SLACK_URL), requestMap, String.class);
    log.info("Response[" + response + "]");
  }

  /**
   * メッセージを通知します.
   * @param slackConfId Slack設定ID.
   * @param message メッセージ.
   * @throws URISyntaxException 例外発生.
   */
  public void notify(
      @NonNull final String slackConfId,
      @NonNull final String message
  ) throws URISyntaxException {
    postMessage(slackConfId, message);
  }
  
  /**
   * 課題が新規作成された旨を通知
   * @param slackConfId Slack設定ID.
   * @param param パラメータ.
   * @throws URISyntaxException 例外発生.
   */
  public void notify(
      @NonNull final String slackConfId,
      @NonNull final IssueCreatedParam param) throws URISyntaxException {
    String message = String.format("`%s` に課題が作成されました。\n"
                                  + ">>> "
                                  + "%s\n"
                                  + "%s\n"
                                  + "%s\n",
                                  param.getRepository().getFullName(),
                                  param.getIssue().getTitle(),
                                  param.getActor().getDisplayName(),
                                  param.getIssue().getLinks().getHtml().getHref());
    postMessage(slackConfId, message);
  }
  
  /**
   * 課題が更新された旨を通知
   * @param slackConfId Slack設定ID.
   * @param param パラメータ.
   * @throws URISyntaxException 例外発生.
   */
  public void notify(
      @NonNull final String slackConfId,
      @NonNull final IssueUpdatedParam param) throws URISyntaxException {
    String message = String.format("`%s` の課題が更新されました。\n"
                                  + ">>> "
                                  + "%s\n"
                                  + "%s\n"
                                  + "%s\n",
                                  param.getRepository().getFullName(),
                                  param.getIssue().getTitle(),
                                  param.getActor().getDisplayName(),
                                  param.getIssue().getLinks().getHtml().getHref());
    postMessage(slackConfId, message);
  }
  
  /**
   * プルリクエストが新規作成された旨を通知
   * @param slackConfId Slack設定ID.
   * @param param パラメータ.
   * @throws URISyntaxException 例外発生.
   */
  public void notify(
      @NonNull final String slackConfId,
      @NonNull final PullRequestCreatedParam param) throws URISyntaxException {
    String message = String.format("`%s` にプルリクエストが作成されました。\n"
                                  + ">>> "
                                  + "%s\n"
                                  + "%s\n"
                                  + "%s\n",
                                  param.getRepository().getFullName(),
                                  param.getPullrequest().getTitle(),
                                  param.getActor().getDisplayName(),
                                  param.getRepository().getLinks().getHtml().getHref());
    postMessage(slackConfId, message);
  }
  
  /**
   * プルリクエストが承認された旨を通知
   * @param slackConfId Slack設定ID.
   * @param param パラメータ.
   * @throws URISyntaxException 例外発生.
   */
  public void notify(
      @NonNull final String slackConfId,
      @NonNull final PullRequestApprovedParam param) throws URISyntaxException {
    String message = String.format("`%s` のプルリクエストが `%s` に承認されました。\n"
                                  + ">>> "
                                  + "%s\n"
                                  + "%s\n",
                                  param.getRepository().getFullName(),
                                  param.getApproval().getUser().getDisplayName(),
                                  param.getPullrequest().getTitle(),
                                  param.getRepository().getLinks().getHtml().getHref());
    postMessage(slackConfId, message);
  }
  
  /**
   * プルリクエストコメントが作成された旨を通知
   * @param slackConfId Slack設定ID.
   * @param param パラメータ.
   * @throws URISyntaxException 例外発生.
   */
  public void notify(
      @NonNull final String slackConfId,
      @NonNull final PullRequestCommentCreatedParam param) throws URISyntaxException {
    String message = String.format("`%s` のプルリクエストに `%s` がコメントしました。\n"
                                  + ">>> "
                                  + "%s\n"
                                  + "%s\n"
                                  + "%s\n",
                                  param.getRepository().getFullName(),
                                  param.getActor().getDisplayName(),
                                  param.getPullrequest().getTitle(),
                                  param.getRepository().getLinks().getHtml().getHref(),
                                  param.getComment().getContent().getRaw());
    postMessage(slackConfId, message);
  }
}
