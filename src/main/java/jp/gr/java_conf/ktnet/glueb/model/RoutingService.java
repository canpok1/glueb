package jp.gr.java_conf.ktnet.glueb.model;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import jp.gr.java_conf.ktnet.glueb.constant.RouteType;
import jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket.WebhookEventType;
import jp.gr.java_conf.ktnet.glueb.model.entity.db.ChatworkConf;
import jp.gr.java_conf.ktnet.glueb.model.entity.db.Route;
import jp.gr.java_conf.ktnet.glueb.model.repository.RouteRepository;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * 受信した情報を適切なルートで処理するためのサービスクラスです.
 * @author tanabe
 *
 */
@Service
@Slf4j
public class RoutingService {
  
  @Autowired
  private RouteRepository routeRepository;

  @Autowired
  private SlackService slackService;
  @Autowired
  private ChatworkService chatworkService;

  @Autowired
  private ParamConvertService paramConvertService;
  
  /**
   * Bitbucketからの情報を処理します.
   * @param entranceId ID.
   * @param eventKey イベントキー.
   * @param param パラメータ.
   * @throws IOException 処理失敗の場合.
   * @throws URISyntaxException 処理失敗の場合.
   * @throws JsonMappingException 処理失敗の場合.
   * @throws JsonParseException 処理失敗の場合.
   */
  public void processForBitbucket(
      @NonNull String entranceId,
      @NonNull String eventKey,
      @NonNull String param
  ) throws JsonParseException, JsonMappingException, URISyntaxException, IOException {
    List<Route> routeList
        = routeRepository.findByEntrance(entranceId, RouteType.BITBUCKET.getTypeValue());
    if (routeList.isEmpty()) {
      log.warn("対応するルート設定なし[id={}]", entranceId);
      return;
    }
    
    for (Route route : routeList) {
      RouteType exitType = RouteType.toEnum(route.getExitType());
      log.info(
          "Route[{}(id={}) → {}(id={})]",
          RouteType.BITBUCKET.toString(),
          entranceId,
          exitType.toString(),
          route.getExitId()
      );
      
      if (exitType == RouteType.SLACK) {
        WebhookEventType eventType = WebhookEventType.toEnum(eventKey);
        switch (eventType) {
          case ISSUE_CREATED:
            slackService.notify(
                route.getExitId(),
                paramConvertService.convertToIssueCreatedParam(param));
            break;
          case ISSUE_UPDATED:
            slackService.notify(
                route.getExitId(),
                paramConvertService.convertToIssueUpdatedParam(param));
            break;
          case PULL_REQUEST_CREATED:
            slackService.notify(
                route.getExitId(),
                paramConvertService.convertToPullRequestCreatedParam(param));
            break;
          case PULL_REQUEST_APPROVED:
            slackService.notify(
                route.getExitId(),
                paramConvertService.convertToPullRequestApprovedParam(param));
            break;
          case PULL_REQUEST_COMMENT_CREATED:
            slackService.notify(
                route.getExitId(),
                paramConvertService.convertToPullRequestCommentCreatedParam(param));
            break;
          default:
            String message = String.format(
                              "Webhook受信[%s(%s)]\n```\n%s\n```",
                              eventType.getEventName(),
                              eventKey,
                              param);
            log.warn("受信したWebhookの解析に失敗");
            slackService.notify(route.getExitId(), message);
            break;
        }
        continue;
      }
      
      if (exitType == RouteType.CHATWORK) {
        WebhookEventType eventType = WebhookEventType.toEnum(eventKey);
        switch (eventType) {
          case ISSUE_CREATED:
            chatworkService.notify(
                route.getExitId(),
                paramConvertService.convertToIssueCreatedParam(param));
            break;
          case ISSUE_UPDATED:
            chatworkService.notify(
                route.getExitId(),
                paramConvertService.convertToIssueUpdatedParam(param));
            break;
          case PULL_REQUEST_CREATED:
            chatworkService.notify(
                route.getExitId(),
                paramConvertService.convertToPullRequestCreatedParam(param));
            break;
          case PULL_REQUEST_APPROVED:
            chatworkService.notify(
                route.getExitId(),
                paramConvertService.convertToPullRequestApprovedParam(param));
            break;
          case PULL_REQUEST_COMMENT_CREATED:
            chatworkService.notify(
                route.getExitId(),
                paramConvertService.convertToPullRequestCommentCreatedParam(param));
            break;
          default:
            String message = String.format(
                              "Webhook受信[%s(%s)]\n```\n%s\n```",
                              eventType.getEventName(),
                              eventKey,
                              param);
            log.warn("受信したWebhookの解析に失敗");
            chatworkService.notify(route.getExitId(), message);
            break;
        }
        continue;
      }
      
      log.warn("出口側種別が未対応[{}]", route.getExitType());
    }
    
  }
  
}
