package jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RepositoryParam {
  private String name;
  
  @JsonProperty("full_name")
  private String fullName;
  
  private LinksParam links;
}
