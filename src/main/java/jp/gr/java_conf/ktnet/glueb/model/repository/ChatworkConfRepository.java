package jp.gr.java_conf.ktnet.glueb.model.repository;

import jp.gr.java_conf.ktnet.glueb.model.entity.db.ChatworkConf;
import jp.gr.java_conf.ktnet.glueb.model.repository.mapper.ChatworkConfMapper;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * chatwork_confテーブルの情報を操作するクラスです.
 * @author tanabe
 */
@Repository
public class ChatworkConfRepository {
  
  @Autowired
  private ChatworkConfMapper mapper;
  
  /**
   * 全レコードを取得します.
   * @return 全レコード.
   */
  public List<ChatworkConf> findAll() {
    return mapper.findAll();
  }
  
  /**
   * 指定したIDのレコードを取得します.
   * @param id ID.
   * @return レコード.
   */
  public ChatworkConf findById(
      @NonNull final String id
  ) {
    return mapper.findById(id);
  }
}
