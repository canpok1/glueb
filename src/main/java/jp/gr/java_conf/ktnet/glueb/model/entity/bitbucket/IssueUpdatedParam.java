package jp.gr.java_conf.ktnet.glueb.model.entity.bitbucket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class IssueUpdatedParam {
  private OwnerParam actor;
  private IssueParam issue;
  private RepositoryParam repository;
  private CommentParam comment;
  private Map<String, SubParam> changes;
  
  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class SubParam {
    @JsonProperty("old") private String oldValue;
    @JsonProperty("new") private String newValue;
  }
}
