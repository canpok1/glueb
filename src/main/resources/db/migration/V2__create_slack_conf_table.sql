CREATE TABLE glueb.slack_conf (
    id varchar(36),
    api_token text NOT NULL,
    channel_name text NOT NULL,
    icon_emoji text,
    username text,
    created timestamp NOT NULL,
    updated timestamp NOT NULL,
    PRIMARY KEY (id)
);
COMMENT ON TABLE glueb.slack_conf IS 'Slack設定テーブル
Slack関連の設定を格納します。';
COMMENT ON COLUMN glueb.slack_conf.id IS '設定値のID';
COMMENT ON COLUMN glueb.slack_conf.api_token IS 'SlackAPIのトークン文字列';
COMMENT ON COLUMN glueb.slack_conf.channel_name IS 'チャネル名';
COMMENT ON COLUMN glueb.slack_conf.icon_emoji IS '絵文字指定のアイコン';
COMMENT ON COLUMN glueb.slack_conf.username IS 'ユーザー名';
COMMENT ON COLUMN glueb.slack_conf.created IS '作成日時';
COMMENT ON COLUMN glueb.slack_conf.updated IS '更新日時';
