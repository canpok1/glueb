CREATE TABLE glueb.chatwork_conf (
    id varchar(36),
    api_token text NOT NULL,
    room_id text NOT NULL,
    created timestamp NOT NULL DEFAULT NOW(),
    updated timestamp NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id)
);
COMMENT ON TABLE glueb.chatwork_conf IS 'Chatwork設定テーブル
Chatwork関連の設定を格納します。';
COMMENT ON COLUMN glueb.chatwork_conf.id IS '設定値のID';
COMMENT ON COLUMN glueb.chatwork_conf.api_token IS 'ChatworkAPIのトークン文字列';
COMMENT ON COLUMN glueb.chatwork_conf.room_id IS 'ルーム名';
COMMENT ON COLUMN glueb.chatwork_conf.created IS '作成日時';
COMMENT ON COLUMN glueb.chatwork_conf.updated IS '更新日時';
