CREATE TABLE glueb.route (
    entrance_id varchar(36),
    entrance_type int,
    exit_id varchar(36),
    exit_type int,
    created timestamp NOT NULL,
    updated timestamp NOT NULL,
    PRIMARY KEY (entrance_id, exit_id)
);
COMMENT ON TABLE glueb.route IS 'ルートテーブル
サービス間の情報の流し方を格納します。';
COMMENT ON COLUMN glueb.route.entrance_id IS '入口側ID';
COMMENT ON COLUMN glueb.route.entrance_type IS '入口側種別
0:Bitbucket
1:Trello
2:Slack
3:Chatwork';
COMMENT ON COLUMN glueb.route.exit_id IS '出口側ID';
COMMENT ON COLUMN glueb.route.exit_type IS '出口側種別
種別値は入口側と同様';
COMMENT ON COLUMN glueb.route.created IS '作成日時';
COMMENT ON COLUMN glueb.route.updated IS '更新日時';
